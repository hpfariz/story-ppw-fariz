from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def CV_page(request):
    return render(request, 'CV_page.html')
